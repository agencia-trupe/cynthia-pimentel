(function($){

    $(document).ready(function() {

        $('#slideshow').cycle({
            fx: 'fade',
            speed: 'slow',
            slides: '> div',
            pager: '#slideshow-pager',
            pagerTemplate: '<a href="#">{{slideNum}}</a>'
        });

        var categoriaProjetosAtiva = $('.projetos-categorias a.active');
        $('.projetos-categorias a').hover(function (){
            categoriaProjetosAtiva.css('border-color','transparent');
        }, function(){
            categoriaProjetosAtiva.css('border-color', '#fff');
        });

        var categoriaMidiaAtiva = $('.midias-categorias a.active');
        $('.midias-categorias a').hover(function (){
            categoriaMidiaAtiva.css('color', '#fff');
        }, function(){
            categoriaMidiaAtiva.css('color', '#5c4f41');
        });

        $('.clipping-thumb, .lightbox').fancybox({
            padding: 0,
            maxWidth: '90%',
            fitToView: false
        });

        $('#contato-form').submit(function(event) {
            event.preventDefault();
            $(this).fadeOut('slow', function() {
                $('.response').fadeIn();
            });
        });

        $('.hamburger').click(function(event) {
            event.preventDefault();
            $('.mobile-menu-wrapper').fadeIn('fast');
        });

        $('.mobile-menu .fechar').click(function(event) {
            event.preventDefault();
            $('.mobile-menu-wrapper').fadeOut('fast');
        });

        function resizeHome() {
            if ($(window).height() < 350) {
              $('footer').addClass('mobile-horz');
            } else {
              $('footer').removeClass('mobile-horz');
            }
        }
        $(window).resize(resizeHome).trigger('resize');

    });

}(jQuery));