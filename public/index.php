<?php

if (isset($_GET['url'])) {

    $view = $_GET['url'];

    if (!is_file('views/'. $view .'.php'))
        $view = 'home';

} else {

    $view = 'home';

}

require 'views/templates/header.php';
require 'views/'. $view .'.php';
require 'views/templates/footer.php';