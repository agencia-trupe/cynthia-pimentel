    <div id="blog" class="centro">
        <h1 class="titulo">Blog</h1>

        <div class="blog-left">
            <div class="blog-post">
                <h2>
                    <a href="blog-post">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem reprehenderit nobis!</a>
                </h2>
                <p class="post-info">
                    <a href="#">Categoria</a> · <span>16 de janeiro de 2015</span> · <a href="#">3 comentários</a>
                </p>
                <img src="assets/img/blog/blog1.jpg" alt="">
                <p class="olho">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, impedit aliquid! Dignissimos in totam blanditiis nulla veritatis tempora, adipisci debitis? Ipsa, nulla quisquam. Facere numquam, magnam corrupti praesentium perspiciatis vel!
                </p>
                <a href="blog-post" class="leia-mais">Leia mais</a>
            </div>
            <div class="blog-post">
                <h2>
                    <a href="blog-post">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem reprehenderit nobis!</a>
                </h2>
                <p class="post-info">
                    <a href="#">Categoria</a> · <span>16 de janeiro de 2015</span> · <a href="#">1 comentários</a>
                </p>
                <p class="olho">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, impedit aliquid! Dignissimos in totam blanditiis nulla veritatis tempora, adipisci debitis? Ipsa, nulla quisquam. Facere numquam, magnam corrupti praesentium perspiciatis vel!
                </p>
                <a href="blog-post" class="leia-mais">Leia mais</a>
            </div>
            <div class="blog-post">
                <h2>
                    <a href="blog-post">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem reprehenderit nobis!</a>
                </h2>
                <p class="post-info">
                    <a href="#">Categoria</a> · <span>16 de janeiro de 2015</span> · <a href="#">Nenhum comentário</a>
                </p>
                <img src="assets/img/blog/blog2.jpg" alt="">
                <p class="olho">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>
                <a href="blog-post" class="leia-mais">Leia mais</a>
            </div>
            <div class="blog-post">
                <h2>
                    <a href="blog-post">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates inventore dolor illo voluptatum beatae dignissimos odit ducimus!</a>
                </h2>
                <p class="post-info">
                    <a href="#">Categoria</a> · <span>16 de janeiro de 2015</span> · <a href="#">4 comentários</a>
                </p>
                <p class="olho">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, impedit aliquid! Dignissimos in totam blanditiis nulla veritatis tempora, adipisci debitis? Ipsa, nulla quisquam. Facere numquam, magnam corrupti praesentium perspiciatis vel!
                </p>
                <a href="blog-post" class="leia-mais">Leia mais</a>
            </div>
            <div class="blog-post">
                <h2>
                    <a href="blog-post">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem reprehenderit nobis!</a>
                </h2>
                <p class="post-info">
                    <a href="#">Categoria</a> · <span>16 de janeiro de 2015</span> · <a href="#">8 comentários</a>
                </p>
                <p class="olho">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, impedit aliquid!
                </p>
                <a href="blog-post" class="leia-mais">Leia mais</a>
            </div>

            <div class="blog-paginacao">
                <a href="#" class="anteriores">« Anteriores</a>
                <a href="#" class="proximos">Próximos »</a>
            </div>
        </div>

        <div class="sidebar">
            <form action="#" id="blog-busca">
                <input name="busca" type="text" placeholder="Pesquisar no Blog" required>
                <input type="submit">
            </form>

            <div class="sidebar-list">
                <h4>Categorias</h4>
                <ul>
                    <li><a href="#">Lorem ipsum dolor sit amet consectetur</a></li>
                    <li><a href="#">Voluptates iusto</a></li>
                    <li><a href="#">Consectetur adipisicing</a></li>
                    <li><a href="#">Commodi minima</a></li>
                    <li><a href="#">Aperiam soluta illo</a></li>
                </ul>
            </div>

            <div class="sidebar-list">
                <h4>Arquivo</h4>
                <ul>
                    <li><a href="#">Janeiro 2015</a></li>
                    <li><a href="#">Dezembro 2014</a></li>
                    <li><a href="#">Novembro 2014</a></li>
                    <li><a href="#">Outubro 2014</a></li>
                    <li><a href="#">Setembro 2014</a></li>
                </ul>
            </div>
        </div>
    </div>