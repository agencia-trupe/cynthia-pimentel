    <div id="contato" class="centro">
        <h1 class="titulo">Contato</h1>

        <form action="#" id="contato-form">
            <input name="nome" id="nome" type="text" placeholder="Nome" required>
            <input name="email" id="email" type="email" placeholder="E-mail" required>
            <input name="telefone" id="telefone" type="text" placeholder="Telefone">
            <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
            <button type="send" class="submit">Enviar</button>
        </form>

        <div class="response">
            <p>Sua mensagem foi enviada com sucesso!</p>
        </div>

        <div class="contato-info">
            <p class="label">Telefone</p>
            <p class="telefone">+55 11 3078 0177</p>
            <p class="label">E-mail</p>
            <a href="mailto:cynthia@cynthiapimentelduarte.com.br" class="email">cynthia@cynthiapimentelduarte.com.br</a>

            <nav class="social">
                <a href="#" class="twitter" target="_blank">Twitter</a>
                <a href="#" class="facebook" target="_blank">Facebook</a>
                <a href="#" class="instagram" target="_blank">Instagram</a>
                <a href="#" class="linkedin" target="_blank">Linkedin</a>
            </nav>
        </div>
    </div>