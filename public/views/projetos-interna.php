    <div id="projetos-interna" class="centro">
        <h1 class="titulo">Projetos</h1>

        <div class="right">
            <h3>Caçapava - Suíte Nina</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam, ea fugiat, sed, cum perspiciatis reprehenderit esse deleniti autem atque saepe nobis doloremque. Id, temporibus!</p>

            <a href="projetos">Voltar a todos os projetos</a>
        </div>

        <div class="left">
            <a class="lightbox" rel="group" href="assets/img/projetos/projeto-interna.jpg">
                <img src="assets/img/projetos/projeto-interna.jpg" alt="">
            </a>
            <a class="lightbox" rel="group" href="assets/img/projetos/projeto-interna2.jpg">
                <img src="assets/img/projetos/projeto-interna2.jpg" alt="">
            </a>
        </div>

        <a href="projetos" class="voltar-mobile">Voltar a todos os projetos</a>

    </div>