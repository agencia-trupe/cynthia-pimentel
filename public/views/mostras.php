    <div id="mostras" class="centro">
        <h1 class="titulo">Mostras</h1>

        <div class="mostras-lista">
            <a href="mostras-interna" class="mostras-thumb">
                <img src="assets/img/mostras/mostra-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <a href="mostras-interna" class="mostras-thumb">
                <img src="assets/img/mostras/mostra-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <a href="mostras-interna" class="mostras-thumb">
                <img src="assets/img/mostras/mostra-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <a href="mostras-interna" class="mostras-thumb">
                <img src="assets/img/mostras/mostra-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <a href="mostras-interna" class="mostras-thumb">
                <img src="assets/img/mostras/mostra-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <a href="mostras-interna" class="mostras-thumb">
                <img src="assets/img/mostras/mostra-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
        </div>

        <div class="paginacao">
            <a href="#" class="active">1</a>
            <a href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
        </div>
    </div>