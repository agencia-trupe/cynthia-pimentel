    <div id="midia" class="centro">
        <h1 class="titulo">Mídia</h1>

        <nav class="midias-categorias">
            <a href="midia" class="active">Clipping</a>
            <a href="midia-materias">Matérias</a>
            <a href="midia-videos">Vídeos</a>
        </nav>

        <div class="clipping-lista">
            <a href="assets/img/clipping/clipping-interna.jpg" rel="clipping1" class="clipping-thumb">
                <img src="assets/img/clipping/clipping-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <div class="hidden-gallery">
                <a href="assets/img/clipping/clipping-interna2.jpg" class="clipping-thumb" rel="clipping1"></a>
            </div>

            <a href="assets/img/clipping/clipping-interna.jpg" rel="clipping1" class="clipping-thumb">
                <img src="assets/img/clipping/clipping-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <div class="hidden-gallery">
                <a href="assets/img/clipping/clipping-interna2.jpg" class="clipping-thumb" rel="clipping1"></a>
            </div>

            <a href="assets/img/clipping/clipping-interna.jpg" rel="clipping1" class="clipping-thumb">
                <img src="assets/img/clipping/clipping-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <div class="hidden-gallery">
                <a href="assets/img/clipping/clipping-interna2.jpg" class="clipping-thumb" rel="clipping1"></a>
            </div>

            <a href="assets/img/clipping/clipping-interna.jpg" rel="clipping1" class="clipping-thumb">
                <img src="assets/img/clipping/clipping-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <div class="hidden-gallery">
                <a href="assets/img/clipping/clipping-interna2.jpg" class="clipping-thumb" rel="clipping1"></a>
            </div>

            <a href="assets/img/clipping/clipping-interna.jpg" rel="clipping1" class="clipping-thumb">
                <img src="assets/img/clipping/clipping-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <div class="hidden-gallery">
                <a href="assets/img/clipping/clipping-interna2.jpg" class="clipping-thumb" rel="clipping1"></a>
            </div>

            <a href="assets/img/clipping/clipping-interna.jpg" rel="clipping1" class="clipping-thumb">
                <img src="assets/img/clipping/clipping-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <div class="hidden-gallery">
                <a href="assets/img/clipping/clipping-interna2.jpg" class="clipping-thumb" rel="clipping1"></a>
            </div>

            <a href="assets/img/clipping/clipping-interna.jpg" rel="clipping1" class="clipping-thumb">
                <img src="assets/img/clipping/clipping-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <div class="hidden-gallery">
                <a href="assets/img/clipping/clipping-interna2.jpg" class="clipping-thumb" rel="clipping1"></a>
            </div>

            <a href="assets/img/clipping/clipping-interna.jpg" rel="clipping1" class="clipping-thumb">
                <img src="assets/img/clipping/clipping-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <div class="hidden-gallery">
                <a href="assets/img/clipping/clipping-interna2.jpg" class="clipping-thumb" rel="clipping1"></a>
            </div>

            <a href="assets/img/clipping/clipping-interna.jpg" rel="clipping1" class="clipping-thumb">
                <img src="assets/img/clipping/clipping-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <div class="hidden-gallery">
                <a href="assets/img/clipping/clipping-interna2.jpg" class="clipping-thumb" rel="clipping1"></a>
            </div>

            <a href="assets/img/clipping/clipping-interna.jpg" rel="clipping1" class="clipping-thumb">
                <img src="assets/img/clipping/clipping-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <div class="hidden-gallery">
                <a href="assets/img/clipping/clipping-interna2.jpg" class="clipping-thumb" rel="clipping1"></a>
            </div>
        </div>

        <div class="paginacao">
            <a href="#" class="active">1</a>
            <a href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
        </div>
    </div>