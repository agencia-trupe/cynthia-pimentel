    <div id="blog" class="centro">
        <h1 class="titulo">Blog</h1>

        <div class="blog-left">
            <div class="blog-post">
                <h2>
                    <a href="blog-post">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem reprehenderit nobis!</a>
                </h2>
                <p class="post-info">
                    <a href="#">Categoria</a> · <span>16 de janeiro de 2015</span> · <a href="#">3 comentários</a>
                </p>
                <img src="assets/img/blog/blog1.jpg" alt="">
                <p class="olho-interna">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, impedit aliquid! Dignissimos in totam blanditiis nulla veritatis tempora, adipisci debitis? Ipsa, nulla quisquam. Facere numquam, magnam corrupti praesentium perspiciatis vel!
                </p>
                <p class="texto">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur autem, libero hic aut pariatur, nulla perspiciatis perferendis quo similique! Magnam veniam, ipsum molestiae? Reiciendis tenetur labore expedita ipsam, aspernatur vitae impedit in inventore! Voluptatibus, necessitatibus pariatur repudiandae. Non saepe, unde fugit ipsam, eum quas illum numquam dolor, soluta necessitatibus perspiciatis! Vel fuga commodi magnam assumenda debitis asperiores voluptatem dicta dolorum ex aperiam corporis doloremque suscipit, similique nostrum sed, porro consequuntur nulla, saepe repudiandae! Qui nulla maiores commodi autem vitae quia, facilis dolore eos similique. Rem itaque eaque, natus quam temporibus sit? Officiis reprehenderit nobis necessitatibus quasi cumque exercitationem quas eligendi! Voluptatem repudiandae quasi numquam laudantium nostrum voluptate suscipit doloribus sequi tempore, rerum, maxime quaerat maiores dicta, recusandae soluta eveniet quae vero aut repellendus porro pariatur. Nam vero ullam iure aspernatur ipsam officiis, nobis ab accusantium odit non obcaecati dolore magnam eaque voluptatum fugiat provident, dolorem necessitatibus. Ipsa saepe, odio nobis nam soluta autem distinctio hic quisquam et eligendi earum suscipit dolores harum, ex. Veniam asperiores totam consectetur veritatis in debitis maiores, dolorem corrupti maxime repellat! Minima similique explicabo, culpa officia eligendi eaque. Necessitatibus, molestiae et quaerat molestias. Architecto nobis ipsam ut sapiente, tempora, quas asperiores adipisci accusamus neque voluptas deleniti.</p>
                <p class="texto">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, eaque, ex? Consequuntur, totam laborum debitis, fuga temporibus nam enim, adipisci fugit eligendi nisi corporis. Nihil officia quis recusandae voluptate, repellat assumenda voluptas veniam illo voluptatibus!</p>
                <img src="assets/img/blog/blog2.jpg" alt="">
                <p class="texto">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa nihil minus ducimus, reprehenderit recusandae illum repudiandae commodi voluptates, voluptatem dicta exercitationem dolorem illo repellendus! Error, itaque incidunt perspiciatis, deleniti velit deserunt, voluptatibus nemo pariatur enim placeat quo voluptatum fuga ducimus praesentium totam. Numquam recusandae inventore nisi laborum iure et, nesciunt expedita porro, tempore enim incidunt, fugit autem illum rerum ad! Omnis eius, beatae vel ab maxime quibusdam, officia perferendis enim quas, veritatis eveniet. Vero, nesciunt, soluta. Placeat quia accusantium sapiente, necessitatibus culpa laborum. Optio dolores aperiam est eos possimus porro reprehenderit similique. Nihil, ut nisi officia sint? Nihil illum, quae?</p>
            </div>

            <div class="comentarios-lista">
                <h4>3 Comentários</h4>
                <div class="comentario">
                    <p class="info">Nome da Pessoa · 16 de Janeiro de 2015</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis ullam excepturi porro quod debitis ut ipsa inventore, vero ipsum rem, saepe illum minus alias illo. Veritatis officia magnam quae culpa quidem totam blanditiis beatae maxime.</p>
                </div>
                <div class="comentario">
                    <p class="info">Nome da Pessoa · 16 de Janeiro de 2015</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis ullam excepturi porro quod debitis ut ipsa inventore, vero ipsum rem, saepe illum minus alias illo. Veritatis officia magnam quae culpa quidem totam blanditiis beatae maxime.</p>
                </div>
                <div class="comentario">
                    <p class="info">Nome da Pessoa · 16 de Janeiro de 2015</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis ullam excepturi porro quod debitis ut ipsa inventore, vero ipsum rem, saepe illum minus alias illo. Veritatis officia magnam quae culpa quidem totam blanditiis beatae maxime.</p>
                </div>
            </div>

            <form action="#" id="blog-comentario">
                <input name="nome" type="text" placeholder="Nome" required>
                <input name="email" type="email" placeholder="E-mail" required>
                <textarea name="mensagem" placeholder="Comentário" required></textarea>
                <button type="send" class="submit">Comentar</button>
            </form>

            <div class="blog-voltar">
                <a href="blog">« Voltar</a>
            </div>
        </div>

        <div class="sidebar">
            <form action="#" id="blog-busca">
                <input name="busca" type="text" placeholder="Pesquisar no Blog" required>
                <input type="submit">
            </form>

            <div class="sidebar-list">
                <h4>Categorias</h4>
                <ul>
                    <li><a href="#">Lorem ipsum dolor sit amet consectetur</a></li>
                    <li><a href="#">Voluptates iusto</a></li>
                    <li><a href="#">Consectetur adipisicing</a></li>
                    <li><a href="#">Commodi minima</a></li>
                    <li><a href="#">Aperiam soluta illo</a></li>
                </ul>
            </div>

            <div class="sidebar-list">
                <h4>Arquivo</h4>
                <ul>
                    <li><a href="#">Janeiro 2015</a></li>
                    <li><a href="#">Dezembro 2014</a></li>
                    <li><a href="#">Novembro 2014</a></li>
                    <li><a href="#">Outubro 2014</a></li>
                    <li><a href="#">Setembro 2014</a></li>
                </ul>
            </div>
        </div>
    </div>