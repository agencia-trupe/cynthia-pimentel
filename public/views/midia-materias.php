    <div id="midia" class="centro">
        <h1 class="titulo">Mídia</h1>

        <nav class="midias-categorias">
            <a href="midia">Clipping</a>
            <a href="midia-materias" class="active">Matérias</a>
            <a href="midia-videos">Vídeos</a>
        </nav>

        <div class="materias-lista">
            <a href="#">
                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iste, accusantium vel</h3>
                <p class="descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe eligendi eos enim fugit. Temporibus quibusdam laboriosam labore, dignissimos in excepturi beatae quaerat deleniti similique? Voluptatum, neque esse quas nemo quisquam!</p>
                <p class="materia-completa">Leia matéria completa</p>
            </a>

            <a href="#">
                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iste, accusantium vel</h3>
                <p class="descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe eligendi eos enim fugit. Temporibus quibusdam laboriosam labore, dignissimos in excepturi beatae quaerat deleniti similique? Voluptatum, neque esse quas nemo quisquam!</p>
                <p class="materia-completa">Leia matéria completa</p>
            </a>

            <a href="#">
                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iste, accusantium vel</h3>
                <p class="descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe eligendi eos enim fugit. Temporibus quibusdam laboriosam labore, dignissimos in excepturi beatae quaerat deleniti similique? Voluptatum, neque esse quas nemo quisquam!</p>
                <p class="materia-completa">Leia matéria completa</p>
            </a>

            <a href="#">
                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius iste, accusantium vel</h3>
                <p class="descricao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe eligendi eos enim fugit. Temporibus quibusdam laboriosam labore, dignissimos in excepturi beatae quaerat deleniti similique? Voluptatum, neque esse quas nemo quisquam!</p>
                <p class="materia-completa">Leia matéria completa</p>
            </a>
        </div>

        <div class="paginacao">
            <a href="#" class="active">1</a>
            <a href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
        </div>
    </div>