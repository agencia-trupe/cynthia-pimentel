<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cynthia Pimentel Duarte - Arquitetura & Design</title>
    <meta name="description" content="Cynthia Pimentel Duarte - Arquitetura & Design">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/main.min.css">
</head>
<body>
    <header>
        <div class="centro">
            <a href="home" class="logo">Cynthia Pimentel Duarte</a>

            <ul class="idiomas">
                <li><a href="#" class="idiomas-en">inglês</a></li>
                <li><a href="#" class="idiomas-pt active">português</a></li>
            </ul>

            <nav class="desktop">
                <a href="perfil"<?=(preg_match('/^perfil/', $view)?' class="active"':'')?>>
                    Perfil
                </a>
                <a href="projetos"<?=(preg_match('/^projetos/', $view)?' class="active"':'')?>>
                    Projetos
                </a>
                <a href="mostras"<?=(preg_match('/^mostras/', $view)?' class="active"':'')?>>
                    Mostras
                </a>
                <a href="midia"<?=(preg_match('/^midia/', $view)?' class="active"':'')?>>
                    Mídia
                </a>
                <a href="blog"<?=(preg_match('/^blog/', $view)?' class="active"':'')?>>
                    Blog
                </a>
                <a href="contato"<?=(preg_match('/^contato/', $view)?' class="active"':'')?>>
                    Contato
                </a>
            </nav>

            <div class="mobile">
                <a href="#" class="hamburger">Menu</a>
                <div class="mobile-menu-wrapper">
                    <nav class="mobile-menu">
                        <a href="#" class="fechar">Fechar</a>
                        <a href="perfil"<?=(preg_match('/^perfil/', $view)?' class="active"':'')?>>Perfil</a>
                        <a href="projetos"<?=(preg_match('/^projetos/', $view)?' class="active"':'')?>>Projetos</a>
                        <a href="mostras"<?=(preg_match('/^mostras/', $view)?' class="active"':'')?>>Mostras</a>
                        <a href="midia"<?=(preg_match('/^midia/', $view)?' class="active"':'')?>>Midia</a>
                        <a href="blog"<?=(preg_match('/^blog/', $view)?' class="active"':'')?>>Blog</a>
                        <a href="contato"<?=(preg_match('/^contato/', $view)?' class="active"':'')?>>Contato</a>
                    </nav>
                </div>
            </div>
        </div>
    </header>
