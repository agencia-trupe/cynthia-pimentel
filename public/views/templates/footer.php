
    <footer<?=($view == 'home' ? ' class="home"' : '')?>>
        <div class="centro">
            <nav class="footer-social">
                <a href="#" class="twitter" target="_blank">Twitter</a>
                <a href="#" class="facebook" target="_blank">Facebook</a>
                <a href="#" class="instagram" target="_blank">Instagram</a>
                <a href="#" class="linkedin" target="_blank">Linkedin</a>
            </nav>

            <p class="footer-tel">+55 11 3078 0177</p>

            <a href="mailto:cynthia@cynthiapimentelduarte.com.br" class="footer-email">cynthia@cynthiapimentelduarte.com.br</a>
        </div>
    </footer>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
    <script src="assets/js/cycle.min.js"></script>
    <script src="assets/js/fancybox.min.js"></script>
    <script src="assets/js/main.min.js"></script>
    <script src="assets/js/placeholder.min.js"></script>
</body>
</html>