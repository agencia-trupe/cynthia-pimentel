    <div id="midia" class="centro">
        <h1 class="titulo">Mídia</h1>

        <nav class="midias-categorias">
            <a href="midia">Clipping</a>
            <a href="midia-materias">Matérias</a>
            <a href="midia-videos" class="active">Vídeos</a>
        </nav>

        <div class="videos-lista">
            <div class="video">
                <div class="video-container">
                    <iframe width="640" height="390" src="//www.youtube.com/embed/zWbDMpYxtIs?showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="video-info">
                    <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, exercitationem</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro cum, ad iste culpa, dolores qui illum doloribus ullam eligendi dolorem debitis nulla iure! Temporibus, culpa.</p>
                </div>
            </div>
            <div class="video">
                <div class="video-container">
                    <iframe width="640" height="390" src="//www.youtube.com/embed/zWbDMpYxtIs?showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="video-info">
                    <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis, exercitationem</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro cum, ad iste culpa, dolores qui illum doloribus ullam eligendi dolorem debitis nulla iure! Temporibus, culpa.</p>
                </div>
            </div>
        </div>

        <div class="paginacao">
            <a href="#" class="active">1</a>
            <a href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
        </div>
    </div>