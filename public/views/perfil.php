    <div id="perfil" class="centro">
        <div class="left">
            <h1 class="titulo">Perfil</h1>

            <p>Cynthia Pimentel Duarte é arquiteta formada pela Universidade Santa Úrsula do Rio de Janeiro. Com escritório em São Paulo desde 2006, atua em projetos residenciais, comerciais, industriais e em design de mobiliário.</p>
            <p>Cynthia tem em seu currículo desde projetos de arquitetura, reforma e decoração de residências a projetos industriais como estações de Metrô e a Casa da Moeda.  Na área comercial realizou projetos e decoração de lojas, consultórios, escritórios, hall de edifícios, tendo projetado apartamentos decorados, áreas comuns e stands de vendas de empreendimentos imobiliários para as empresas Abyara e Agra. Participa de mostras de decoração como Casa Cor, Casa Boa Mesa, Casa Hotel e decorouLounges do Paradise Golf & Lake Resort Mogi das Cruzes para eventos do Family Workshop da LIDE.</p>

            <h3>“Cada projeto procura atender o estilo, as necessidades e sonhos de cada cliente sem abrir mão da beleza, conforto e funcionalidade”</h3>

            <p>Por esta ampla experiência e realizações, seu escritório concilia o lado de criatividade e estética com a disciplina e eficiência do lado industrial da arquitetura. Em consequência uma característica diferencial é o total comprometimento com as necessidades e expectativas dos clientes priorizando o desenvolvimento de projetos bem detalhados em plantas, especificações dos materiais e serviços e em orçamentos, para a perfeita compreensão e o completo aceite pelo cliente, antes da execução. O cumprimento de prazos e orçamentos é uma prioridade e um diferencial do escritório.</p>
            <p>Cynthia Pimentel Duarte Arquitetura & Design ganhou o prêmio com seu projeto para um apartamento no bairro nobre da capital paulista, Vila Nova Conceição, que encantou o júri composto por formadores de opinião do mundo inteiro que atuam nas áreas de arquitetura, design, marketing, comunicação, tecnologia e indústria imobiliária.</p>
            <p>O International Property Awards é aberto para profissionais que atuam nos setores residencial e comercial de todas as partes do globo. Os prêmios celebram os mais altos níveis de realizações de empresas operando em todos os setores de propriedades e imobiliários. Um International Property Award é uma marca de excelência reconhecida mundialmente.</p>
        </div>
        <div class="right">
            <img src="assets/img/perfil/foto-cynthia.png" alt="">
            <img src="assets/img/perfil/premio.png" alt="">
        </div>
    </div>