    <div id="projetos" class="centro">
        <h1 class="titulo">Projetos</h1>

        <nav class="projetos-categorias">
            <a href="#" class="active">Arquitetura</a>
            <a href="#">Interiores</a>
            <a href="#">Corporativo</a>
        </nav>

        <div class="projetos-lista">
            <a href="projetos-interna" class="projetos-thumb">
                <img src="assets/img/projetos/projeto-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <a href="projetos-interna" class="projetos-thumb">
                <img src="assets/img/projetos/projeto-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <a href="projetos-interna" class="projetos-thumb">
                <img src="assets/img/projetos/projeto-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <a href="projetos-interna" class="projetos-thumb">
                <img src="assets/img/projetos/projeto-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <a href="projetos-interna" class="projetos-thumb">
                <img src="assets/img/projetos/projeto-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
            <a href="projetos-interna" class="projetos-thumb">
                <img src="assets/img/projetos/projeto-entrada.jpg" alt="">
                <div class="overlay">
                    <p>Lorem ipsum dolor sit amet consectetur</p>
                </div>
            </a>
        </div>

        <div class="paginacao">
            <a href="#" class="active">1</a>
            <a href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
        </div>
    </div>