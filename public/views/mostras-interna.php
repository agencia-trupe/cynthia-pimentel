    <div id="mostras-interna" class="centro">
        <h1 class="titulo">Mostras</h1>

        <div class="right">
            <h3>Casa Cor 2007 - Boulangerie</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam, ea fugiat, sed, cum perspiciatis reprehenderit esse deleniti autem atque saepe nobis doloremque. Id, temporibus!</p>

            <a href="mostras">Voltar a todas as mostras</a>
        </div>

        <div class="left">
            <a class="lightbox" rel="group" href="assets/img/mostras/mostra-interna.jpg">
                <img src="assets/img/mostras/mostra-interna.jpg" alt="">
            </a>
            <a class="lightbox" rel="group" href="assets/img/mostras/mostra-interna2.jpg">
                <img src="assets/img/mostras/mostra-interna2.jpg" alt="">
            </a>
        </div>

        <a href="mostras" class="voltar-mobile">Voltar a todas as mostras</a>

    </div>